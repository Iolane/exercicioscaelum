package exercicioscaelum;

public class TesteConta {
    
    public static void main(String[] args) {
    /*
    Conta c = new Conta();
    ContaCorrente cc = new ContaCorrente();
    ContaPoupanca cp = new ContaPoupanca();
    */
    Conta c = new Conta();
    Conta cc = new ContaCorrente();
    Conta cp = new ContaPoupanca();
  
    c.deposita(1000); 
    cc.deposita(1000); 
    cp.deposita(1000);
  
    c.atualiza(0.01);
    cc.atualiza(0.01);
    cp.atualiza(0.01);

    System.out.println("conta: "+c.getSaldo());
    System.out.println("conta corrente: " +cc.getSaldo());
    System.out.println("conta poupança: " +cp.getSaldo());

  }
}
